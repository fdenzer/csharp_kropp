﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;

namespace de.fhworms.inf1743.cs.aufgabe1 {


    static class Program {

        #region input methods

        private static int ReadInt() {

            int number = 0;

            while (number < 1583 || number > 9999) {
                Console.Write("Bitte vierstellige Jahreszahl (mindestens 1583) eingeben\n\n==>");
                try {
                    number = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                }
            }
            return number;
        }

        private static char ReadChar() {
            char c;

            Console.WriteLine("Bitte Ausgabeart wählen: [B(ildschirm)/d(atei)]");
            Console.WriteLine("Standard ist Bildschirm, sofern kein kleines 'd' fuer Datei eingegeben wird.");
            Console.Write("\n==>");

            while (true) {
                try {
                    c = char.Parse(Console.ReadLine());
                    return c;
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                }
            }
        }

        #endregion

        /// <summary>
        /// Calculates christian holidays. Shows them on console or saves as text file.
        /// </summary>
        /// <param name="args">not in use</param>
        static void Main(string[] args) {

            #region input from user

            char selection = ReadChar();
            int chosenYear = ReadInt();

            #endregion

            #region processing and output

            #region find earliestEasters

            // temporary helper
            Easter easterSunday;

            // make ArrayList of all earliest EasterSundays
            ArrayList earliestEasters = new ArrayList();

            for (int i = 1583; i <= 4200; i++) {
                easterSunday = new Easter(i);
                if (easterSunday.PointInTime.Day == 22 && easterSunday.PointInTime.Month == 3) {
                    earliestEasters.Add(easterSunday.PointInTime.Year);
                }
            }

            #endregion

            #region find latestEasters

            // check for each year if easter happens on that day. Uses easterSunday from 'find earliestEasters' region
  
            ArrayList latestEasters = new ArrayList();

            for (int i = 1583; i <= 4200; i++) {
                easterSunday = new Easter(i);
                //if (easterSunday.PointInTime.Day == comparedSunday.PointInTime.Day && easterSunday.PointInTime.Month == comparedSunday.PointInTime.Month) {
                if (easterSunday.PointInTime.Day == 25 && 4 == easterSunday.PointInTime.Month) {
                    latestEasters.Add(easterSunday.PointInTime.Year);
                }
            }

            #endregion

            #region conditional output redirection

            if (selection != 'd') {
                // calculate and print to screen

                ChristianHolidays.showDays(chosenYear);
                Console.WriteLine();

                Console.WriteLine("\nOstern liegt in den folgenden Jahren am frühstmöglichen Datum");

                foreach (int year in earliestEasters) {
                    Console.WriteLine("{0}", year);
                }

                Console.WriteLine("\nOstern liegt in den folgenden Jahren am spätesten Datum");

                foreach (int year in latestEasters) {
                    Console.WriteLine("{0}", year);
                }

            } else {
                // send holidays for a year (2012) to file output.txt

                ChristianHolidays.streamDays(chosenYear, "output.txt");

                // send all early easter years to file
                using (StreamWriter myStream = new StreamWriter(new FileStream("lateEaster_25_04_X.txt", FileMode.Create))) {

                    //flush buffer to the underlying stream after call to myStream.Write
                    myStream.AutoFlush = true;
                    Console.SetOut(myStream);

                    Console.WriteLine("Ostern liegt in den folgenden Jahren am frühstmöglichen Datum");

                    foreach (int year in earliestEasters) {
                        myStream.WriteLine("{0}", year);
                    }
                    myStream.Close();
                }

                using (StreamWriter myStream = new StreamWriter(new FileStream("earlyEaster_22_03_X.txt", FileMode.Create))) {

                    Console.WriteLine("Ostern liegt in den folgenden Jahren am spätesten Datum");

                    foreach (int year in latestEasters) {
                        Console.WriteLine("{0}", year);
                    }

                    myStream.Close();
                }

            }

            #endregion

            Console.WriteLine("\nBeliebige Taste zum schließen drücken.");
            Console.ReadKey();

            #endregion
        }
    }
}
