﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace de.fhworms.inf1743.cs.aufgabe1 {
    sealed class Easter {
        
        #region fields

        private DateTime pointInTime;

        #endregion

        #region properties

        public DateTime PointInTime {
            get { return pointInTime; }
            set { pointInTime = value; }
        }

        #endregion

        #region constructor(s)
        public Easter(int year) {
            pointInTime = calcDate(year);
        }

        #endregion

        #region methods

        private DateTime calcDate(int year) {
            // local variables
            int a, b, c, d, e;

            // year is already known, but get the exact day of the year with following formula
            a = year % 4;
            b = year % 7;
            c = year % 19;
            d = ((19 * c) + 15 + (year / 100) - (year / 400) - (year / 300)) % 30;
            e = ((2 * a) + (4 * b) + (6 * d) + 4 + (year / 100) - (year / 400)) % 7;
            if (d == 29 && e == 6) {
                e = -1;
            }
            else if (d == 28 && e == 6 && c > 10) {
                e = -1;
            }

            // easter sunday happens d+e+1 days after 21th of march
            DateTime easter = new DateTime(year, 3, 21);
            easter = easter.AddDays((double)d + e + 1);
            return easter;
        }

        #endregion
        
    }
}
