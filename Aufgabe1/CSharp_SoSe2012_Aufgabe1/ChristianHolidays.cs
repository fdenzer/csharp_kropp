﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace de.fhworms.inf1743.cs.aufgabe1 {

    /// <summary>
    /// Calculates christian holidays depending on easter.
    /// </summary>
    static class ChristianHolidays {

        private static IEnumerable<Tuple<string, int>> daysFromEaster() {
            yield return new Tuple<string, int>("Aschermittwoch", -46);
            yield return new Tuple<string, int>("1. Fastensonntag", -42);
            yield return new Tuple<string, int>("5. Fastensonntag", -14);
            yield return new Tuple<string, int>("Palmsonntag", -7);
            yield return new Tuple<string, int>("Gründonnerstag", -3);
            yield return new Tuple<string, int>("Karfreitag", -2);
            yield return new Tuple<string, int>("Ostersonntag", 0);
            yield return new Tuple<string, int>("Rogate", 35);
            yield return new Tuple<string, int>("Himmelfahrt", 39);
            yield return new Tuple<string, int>("Pfingstsonntag", 49);
            yield return new Tuple<string, int>("Trinitatis", 56);
            yield return new Tuple<string, int>("Fronleichnam", 60);
        }

        /// <summary>
        /// Shows a list of christian holiday dates.
        /// </summary>
        /// <param name="year">the year where the dates shall be calculated</param>
        public static void showDays(int year) {

            Console.WriteLine("Kirchliche Feiertage {0:G}\n", year);

            //MyStream stream = new MyStream();

            Easter thisYearsEaster = new Easter(year);
            DateTime tmp = thisYearsEaster.PointInTime;

            foreach (var holiday in daysFromEaster()) {
                Console.WriteLine("{0,18}{1,14:M}", holiday.Item1, tmp.AddDays(holiday.Item2));
            }
        }

        /// <summary>
        /// Creates a list with a years christian holiday dates as output.
        /// </summary>
        /// <param name="year">the year where the dates shall be calculated</param>
        public static void streamDays(int year, string file) {

            StreamWriter myReturnStream;
            myReturnStream = new StreamWriter(new FileStream(file, FileMode.Create));
            myReturnStream.AutoFlush = true;
            Console.SetOut(myReturnStream);

            Console.WriteLine("Kirchliche Feiertage {0:G}\n", year);

            //MyStream stream = new MyStream();

            Easter thisYearsEaster = new Easter(year);
            DateTime thisYearsDateTime = thisYearsEaster.PointInTime;

            foreach (var holiday in daysFromEaster()) {
                myReturnStream.WriteLine("{0,18}{1,14:M}", holiday.Item1, thisYearsDateTime.AddDays(holiday.Item2));
            }

            myReturnStream.Close();
            //return myReturnStream;
        }
    }
}
