Modifizieren Sie Ihr Programm aus Aufgabe 3 in der Weise, dass der Algorithmus zur Bestimmung der Nullstellen durch Intervallhalbierung in einem eigenen Prozess (Server-Prozess) auf dem gleichen Rechner oder auf einem zweiten Rechner läuft.

## Hinweise

* Definieren Sie die Funktion f(x) in der Client-Assembly. Nutzen Sie ein Delegate zum Aufruf der Funktion vom Server.

* Verwenden Sie zum Aufruf des Nullstellen-Algorithmus vom Client-Prozess Marshal-by-reference.

* Verwenden Sie zur Rückgabe der Ergebnisse vom Server-Prozess Marshal-by-value.

* Definieren und nutzen Sie für den gemeinsamen Zugriff auf die Meta-Daten ein Interface.