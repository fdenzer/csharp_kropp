﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using de.fhworms.inf1743.cs.aufgabe2;
using de.fhworms.inf1743.cs.aufgabe3;
using de.fhworms.inf1743.cs.aufgabe4.common;
using System.Security.Permissions;

namespace de.fhworms.inf1743.cs.aufgabe4.client {
    static class Aufgabe4 {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread][SecurityPermissionAttribute(SecurityAction.Demand, Unrestricted = true)]   // rights?
        static void Main() {

            #region initialization of UI

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            #endregion

            #region initialize network connection

            Client myClient = new Client();
            INetwork remoteObj = myClient.Connect();

            #endregion

            #region IPO - input

            double lowerLimit, upperLimit;

            do {
                lowerLimit = IOHelper.AskForDouble(false);
                upperLimit = IOHelper.AskForDouble(true);

            } while (lowerLimit >= upperLimit);

            #endregion

            #region IPO - processing

            function myFunc = (x) => {
                return (x == 0) ? 30.0d : Math.Sin(x * 30.0d) / x;
            };

            try {
                remoteObj.MyFunc = myFunc;
            } catch (Exception ex) {
                MessageBox.Show(ex.InnerException.Message);
            }

            CalculateValues calcVals = new CalculateValues(myFunc);
            calcVals.FindValuesInRange(lowerLimit, upperLimit);

            #endregion

            #region IPO - output

            // in range [0,1] there are 9 zeros for sin(30x)/x (http://goo.gl/Fk3V5)

            Plot plot = new Plot();
            plot.CreateSeries(true, remoteObj.GetZeros(lowerLimit, upperLimit));
            plot.CreateSeries(false, calcVals.Values);
            Application.Run(plot);

            #endregion

        }
    }
}
