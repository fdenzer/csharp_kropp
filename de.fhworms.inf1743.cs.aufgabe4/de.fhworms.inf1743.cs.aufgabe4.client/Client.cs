﻿using System;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using Microsoft.VisualBasic;
using de.fhworms.inf1743.cs.aufgabe4.common;
using System.Security.Permissions;
using System.Runtime.Serialization.Formatters;
using System.Collections;

namespace de.fhworms.inf1743.cs.aufgabe4.client {
    public sealed class Client {

        public INetwork Connect() {
            BinaryClientFormatterSinkProvider provider = new BinaryClientFormatterSinkProvider();
            IDictionary prop = new Hashtable();
            prop["port"] = 0;
            prop["typeFilterLevel"] = TypeFilterLevel.Full;
            //TcpChannel chan = new TcpChannel(prop, provider, null);
            //TcpChannel chan = new TcpChannel();
            ChannelServices.RegisterChannel(new TcpClientChannel(), true);

            Type reqType = typeof(INetwork);
            INetwork remoteObject = (INetwork)Activator.GetObject(reqType, "tcp://localhost:4711/findRoots");
            //if (remoteObject == null) {
            //    Interaction.MsgBox("Server nicht gefunden.");
            //}
            return remoteObject;
        }
    }
}
