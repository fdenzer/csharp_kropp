﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using de.fhworms.inf1743.cs.aufgabe2;

namespace de.fhworms.inf1743.cs.aufgabe4.common {
    public interface INetwork {
        List<Tuple<double, double>> GetZeros(double lowerLimit, double upperLimit);

        function MyFunc {
            get;
            set;
        }
    }
}
