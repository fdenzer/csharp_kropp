﻿using System;
using System.Runtime.Remoting;
using de.fhworms.inf1743.cs.aufgabe2;
using de.fhworms.inf1743.cs.aufgabe4.common;
using System.Collections.Generic;

namespace de.fhworms.inf1743.cs.aufgabe4.server {
    class NetworkCalcZeros : CalculateZeros, INetwork {

        public List<Tuple<double, double>> GetZeros(double lowerLimit, double upperLimit) {
            FindZerosInRange(lowerLimit, upperLimit);
            return zeros;
        }
    }
}
