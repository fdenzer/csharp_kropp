﻿using System;
using System.Text;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using de.fhworms.inf1743.cs.aufgabe2;
using de.fhworms.inf1743.cs.aufgabe4.common;

namespace de.fhworms.inf1743.cs.aufgabe4.server {
    static class Server {
        static void Main(string[] args) {
            TcpChannel chan = new TcpChannel(8085);
            ChannelServices.RegisterChannel(chan, false);

            Type commonInterfaceType = typeof(NetworkCalcZeros);

            RemotingConfiguration.RegisterWellKnownServiceType(commonInterfaceType, "findRoots", WellKnownObjectMode.SingleCall);
            Console.WriteLine(@"Closing server on any key.");
            Console.ReadKey();
        }
    }
}
