﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace de.fhworms.inf1743.cs.aufgabe2 {

    static class Aufgabe2 {

        private static double ReadDouble() {

            double real;

            while (true) {
                try {
                    real = Convert.ToDouble(Console.ReadLine());
                    return real;
                }
                catch (FormatException e) {
                    Console.Write("{0}\nNochmal bitte: ", e.Message);
                }
            }
        }

        static void Main(string[] args) {

            #region IPO - input

            double lowerLimit, upperLimit;

            do {
                Console.Write("Bitte das untere Intervalllimit (z.B. 0,0) eingeben: ");
                lowerLimit = ReadDouble();

                Console.Write("Bitte das obere Intervalllimit (z.B. 1,0) eingeben: ");
                upperLimit = ReadDouble();

            } while (lowerLimit >= upperLimit);

            #endregion

            #region IPO - processing

            function myFunc = (x) => {
                return (Math.Sign(x) == 0) ? 30.0d : Math.Sin(x * 30.0d) / x;
            };

            CalculateZeros calcZeros = new CalculateZeros();
            calcZeros.MyFunc = myFunc;

            calcZeros.FindZerosInRange(lowerLimit, upperLimit);

            #endregion

            #region IPO - output

            // in range [0,1] there are 9 zeros for sin(30x)/x (http://goo.gl/Fk3V5)

            Console.WriteLine("\nNullstellen:");
            foreach (var item in calcZeros.Zeros) {
                Console.WriteLine("{0}", item);
            }

            Console.WriteLine("Enter zum beenden druecken.");
            Console.Read();

            #endregion
        }
    }
}
