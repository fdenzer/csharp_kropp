﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace de.fhworms.inf1743.cs.aufgabe2 {
    public class CalculateZeros : MarshalByRefObject {

        #region protected attributes

        protected function myFunc;
        protected double segmentWidth, epsilon;
        protected List<Tuple<double, double>> zeros;

        #endregion

        #region property accessors

        public double Epsilon {
            get { return epsilon; }
            set { epsilon = value; }
        }

        public List<Tuple<double, double>> Zeros {
            get { return zeros; }
        }

        public function MyFunc {
            get { return myFunc; }
            set { myFunc = value; }
        }

        public double SegmentWidth {
            get { return segmentWidth; }
            set { segmentWidth = value; }
        }

        #endregion

        #region constructor(s)

        public CalculateZeros() {
            segmentWidth = 0.01d;
            epsilon = 1E-15;
            zeros = new List<Tuple<double, double>>();
            // set myFunc later
            myFunc = null;
        }

        #endregion

        #region methods

        public void FindZerosInRange(double lowerLimit, double upperLimit) {
            double y, x = lowerLimit;
            int k = 1;

            // seems overcomplicated, but k times segmentWidth is more exact than x += segmentWidth
            for (x = lowerLimit; x <= upperLimit; x = lowerLimit + (k++) * segmentWidth) {

                y = myFunc(x);

                if (Math.Sign(y) == 0) {
                    // found zero
                    zeros.Add(new Tuple<double, double>(x, y));

                } else if (Math.Sign(y) == -Math.Sign(myFunc(x + segmentWidth))) {
                    // start of interval bisection
                    Bisect(x, x + segmentWidth);
                }
            }
        }

        private void Bisect(double lowerLimit, double upperLimit) {

            double middle;

            // do the if-part in interval containing zero
            if (Math.Sign(myFunc(lowerLimit)) == -Math.Sign(myFunc(upperLimit))) {

                // checks exit condition: x-values diverge little, making the segment small enough
                middle = (lowerLimit + upperLimit) / 2.0d;
                if (Math.Abs(upperLimit - lowerLimit) <= epsilon) {
                    zeros.Add(new Tuple<double, double>(middle, myFunc(middle)));

                } else {
                    // divide interval and and check smaller sections
                    // first half
                    Bisect(lowerLimit, middle);
                    // second half
                    Bisect(middle, upperLimit);
                }
            }
        }

        #endregion
    }
}