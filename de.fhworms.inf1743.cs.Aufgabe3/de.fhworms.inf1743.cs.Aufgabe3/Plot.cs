﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace de.fhworms.inf1743.cs.aufgabe3 {
    public sealed partial class Plot : Form {

        int seriesCount;

        public int SeriesCount {
            get { return seriesCount; }
        }

        public Plot() {
            InitializeComponent();
            seriesCount = 0;
        }

        /// <summary>
        /// Adds one more scatter plot.
        /// </summary>
        /// <param name="listed">sets x-value-labels and makes marker larger</param>
        /// <param name="values">list of double x-y-coordinates</param>
        public void CreateSeries(bool listed, List<Tuple<double, double>> values) {

            if (seriesCount++ > 0) chart.Series.Add("Series" + seriesCount);

            foreach (var valuePair in values) {
                if (listed) xy_values.Items.Add("Nullstelle: X: " + valuePair.Item1 + " Y: " + valuePair.Item2);
                chart.Series["Series" + seriesCount as String].Points.AddXY(valuePair.Item1, valuePair.Item2);
            }

            // Set point chart type
            chart.Series["Series" + seriesCount as String].ChartType = SeriesChartType.Point;

            // Set marker size
            chart.Series["Series" + seriesCount as String].MarkerSize = 10 + (listed ? 3 : -3);

            // Set marker shape
            chart.Series["Series" + seriesCount as String].MarkerStyle = MarkerStyle.Diamond;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }
    }
}
