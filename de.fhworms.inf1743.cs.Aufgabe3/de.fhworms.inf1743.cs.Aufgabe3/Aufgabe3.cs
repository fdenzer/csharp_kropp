﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using de.fhworms.inf1743.cs.aufgabe2;

namespace de.fhworms.inf1743.cs.aufgabe3 {
    static class Aufgabe3 {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {

            #region initialization of UI

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            #endregion

            #region IPO - input

            double lowerLimit, upperLimit;

            do {
                lowerLimit = IOHelper.AskForDouble(false);
                upperLimit = IOHelper.AskForDouble(true);

            } while (lowerLimit >= upperLimit);

            #endregion

            #region IPO - processing

            function myFunc;

            myFunc = x => {
                return (Math.Sign(x) == 0) ? 30.0d : Math.Sin(x * 30.0d) / x;
            };

            CalculateValues calcVals = new CalculateValues(myFunc);
            CalculateZeros calcZeros = new CalculateZeros();
            calcZeros.MyFunc = myFunc;

            calcZeros.FindZerosInRange(lowerLimit, upperLimit);
            calcVals.FindValuesInRange(lowerLimit, upperLimit);

            #endregion

            #region IPO - output

            // in range [0,1] there are 9 zeros for sin(30x)/x (http://goo.gl/Fk3V5)

            Plot plot = new Plot();
            plot.CreateSeries(true, calcZeros.Zeros);
            plot.CreateSeries(false, calcVals.Values);
            Application.Run(plot);


            #endregion
        }
    }
}
