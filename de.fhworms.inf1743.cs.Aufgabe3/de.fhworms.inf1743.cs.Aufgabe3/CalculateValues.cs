﻿using System;
using System.Collections.Generic;
using de.fhworms.inf1743.cs.aufgabe2;

namespace de.fhworms.inf1743.cs.aufgabe3 {
    public sealed class CalculateValues {

        #region private attributes

        private function myFunc;
        private double segmentWidth;
        private List<Tuple<double, double>> values;

        #endregion

        #region property accessors

        public List<Tuple<double, double>> Values {
            get { return values; }
        }

        public function MyFunc {
            get { return myFunc; }
        }

        public double SegmentWidth {
            get { return segmentWidth; }
            set { segmentWidth = value; }
        }

        #endregion

        #region constructor(s)

        public CalculateValues(function _myFunc) {
            segmentWidth = 0.01d;
            values = new List<Tuple<double, double>>();
            myFunc = _myFunc;
        }

        #endregion

        #region method(s)

        public void FindValuesInRange(double lowerLimit, double upperLimit) {
            double x = lowerLimit;
            int k = 1;

            // seems overcomplicated, but k times segmentWidth is more exact than x += segmentWidth
            for (x = lowerLimit; x <= upperLimit; x = lowerLimit + (k++) * segmentWidth) {

                values.Add(new Tuple<double, double>(x, myFunc(x)));

            }
        }

        #endregion
    }
}